var express = require('express');
var router = express.Router();
var request = require("request");

/* GET users listing. */
router.get('/', function(req, res, next) {
  req.toJade.title = "Users";

  req.api.getUsers({}, function(err, result) {
    if(err) console.log(err);

    req.toJade.users = result;
    res.render('users/user_list', req.toJade);
  });
});

router.get('/:username', function(req, res) {
  req.api.getUserBySearch('nickname: "'+req.params.username+'"', function(err, users) {
    if(err) console.log(err);

    if(users.length > 0){
      var user = users[0];
      console.log(user);
      req.toJade.title = user.name +"'s Profile";
      req.toJade.user = user;

      res.render('users/user_profile', req.toJade);
    }else{
      res.redirect('/users');
    }

  });
});

router.get('/register', function(req, res){
  req.toJade.title = "Register";
  res.render('users/register', req.toJade);
});

module.exports = router;
