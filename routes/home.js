var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  req.toJade.title = "Worker";
  res.render('home', req.toJade);
});

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('https://regisworker.auth0.com/v2/logout?returnTo='+req.protocol + '://' + req.get('host'));
});

module.exports = router;
