var passport = require('passport');

var Auth0Strategy = require('passport-auth0');

var strategy = new Auth0Strategy({
    domain:       'regisworker.auth0.com',
    clientID:     'xPfA76pCGgC9XpDW0ppbUVt2d7WuYvi9',
    clientSecret: 'WjGFkMjXXO0XxU7nN8Eg6uiR',
    callbackURL:  '/callback'
  }, function(accessToken, refreshToken, extraParams, profile, done) {
    // accessToken is the token to call Auth0 API (not needed in the most cases)
    // extraParams.id_token has the JSON Web Token
    // profile has all the information from the user
    console.log(profile);
    return done(null, profile);
  });

passport.use(strategy);

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

module.exports = strategy;
