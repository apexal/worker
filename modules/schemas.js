module.exports = {
  user: {
    username: String,
    email: { type: String, required: true, unique: true },
    rank: Number,
    points: Number,
    loginCount: Number,
    last_login: Date,
    last_point_login: Date,
    preferences: Object
  },
  group: {
    creator: String,
    rank: Number,
    title: String,
    description: String,
    users: Array
  }
}
